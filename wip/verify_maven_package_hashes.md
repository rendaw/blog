# Verify Maven Package Hashes
Maven packages can be signed but I've heard rumors of unreliability, and you can't easily do pinning yourself - you have to trust the version number is credible.

Using `maven-enforcer-plugin` you can hash and verify files, and using the `maven-dependency-plugin` you can get the path of any jar dependency.

```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-dependency-plugin</artifactId>
    <version>2.3</version>
    <executions>
        <execution>
            <id>getClasspathFilenames</id>
            <goals>
                <goal>properties</goal>
            </goals>
        </execution>
    </executions>
</plugin>
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-enforcer-plugin</artifactId>
    <version>3.0.0-M1</version>
    <executions>
        <execution>
            <id>enforce-checksum</id>
            <goals>
                <goal>enforce</goal>
            </goals>
            <phase>process-sources</phase>
            <configuration>
                <rules>
                    <requireFileChecksum>
                        <file>${org.mindrot:jbcrypt:jar}</file>
                        <checksum>
                            95ffe1f6593f83da047ff289a52b5ef73adcaca4a9a990a47744fbd50a52fce39958c17ed393e95d64093b2acbcaba39ba0b5b983a2e0ad98ce5c6e9282a67cd
                        </checksum>
                        <type>sha512</type>
                    </requireFileChecksum>
                </rules>
            </configuration>
        </execution>
    </executions>
</plugin>
```

Add a `<rule>` for each dependency you want to verify.  In the example above I wanted to check
```
<dependency>
    <groupId>org.mindrot</groupId>
    <artifactId>jbcrypt</artifactId>
    <version>0.4</version>
</dependency>
```

If you specify an invalid checksum, the plugin will report the calculated checksum when you build - I used this to get the checksum myself.