The worst thing a language can do is try to force its users to do something.  Make doing the "right thing" easy and beneficial to the user, don't make doign the "wrong thing" artificially difficult.  And if they still don't do it maybe you're wrong.

Encapsulation is about reducing connections between pieces of code to make them easier to reason about.

But encapsulation is a design process - not a result.  Just like you can't predict which exceptions a user cares about, you can't predict which part of a module are important to the user.