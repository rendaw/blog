How to version is fairly well standardized, but when to version is not always clear.  My rules of thumb at the moment:

1. For dependencies you maintain and use in a project that you might want to modify while developing, use submodules to vendor them.  The depending project uses hashes as version ids, and hashes update automatically.
2. Update version ids when you decide to publish changes for other users, when publishing a project that needs them to compile and you don't use submodules yet, or you want to use them for a new small project and can accept publishing delays.
3. Stay at < 1.0.0 until someone other than yourself starts using your software/library.
4. Never hesitate to release a new version.  It doesn't matter if you release 1 or 50 versions a year, as long as every change has a distinct version identifier.

### In Java

In Intellij each dependency should be added as a module to the project.  When building, use an aggregate pom with each dependency and the main project listed as modules (pom packaging type, no parent/children relationship).

If Intellij doesn't detect the changes, the dependency may be overridden by a transitive dependency of another vendored project.  Make sure that the highest dependency version of all modules is the version reported by the module you have local changes in.  If there's a higher version required by some module then the whole project will use that from your m2 repo rather than the Intellij module with local changes.

### Caveats

After publishing changes all vendored copies of the library must be updated or else they will revert to using the Maven Central version (since the dependency version no longer matches).
