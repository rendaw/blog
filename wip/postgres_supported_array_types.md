# Listing PostgreSQL and CockroachDB supported array types
JDBC runs a similar query to look up delimiters and such when creating an array value for a statement.

```
=> SELECT t.oid, t.typname FROM pg_catalog.pg_type t, pg_catalog.pg_type e WHERE t.typelem = e.oid;
 oid  |   typname    
------+--------------
 1003 | _name
 1009 | _text
 1115 | _timestamp
 1005 | _int2
 1007 | _int4
 1231 | _numeric
 1028 | _oid
 1187 | _interval
 1000 | _bool
 1016 | _int8
 1001 | _bytea
 1182 | _date
 1021 | _float4
 1022 | _float8
 1185 | _timestamptz
 2951 | _uuid
   22 | int2vector
 1015 | _varchar
(18 rows)

```

In JDBC at least if the array type isn't supported, you get a `org.postgresql.util.PSQLException: No results were returned by the query.` when running `createArrayOf`.