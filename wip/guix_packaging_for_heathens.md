
So this was fairly simple.  It's just a translation of [Arch AUR's MS branded VS Code package](https://aur.archlinux.org/cgit/aur.git/tree/PKGBUILD?h=visual-studio-code-bin), sans the namespacing directory tweaks (since Guix takes care of namespacing installed packages).

```
(define vscode (package
    (name "vscode")
    (version "1.47.3")
    (source (origin
        (method url-fetch)
        (uri (string-append
            "https://vscode-update.azurewebsites.net/"
            version
            "/linux-x64/stable"
        ))
        (sha256 (base16-string->bytevector "7e8262884322e030a35d3ec111b86b17b31c83496b41e919bd3f0d52abe45898"))
    ))
    (build-system copy-build-system)
    (arguments '(#:install-plan '( ("." "") )))
    (synopsis "")
    (description "")
    (home-page "")
    (license "")
))
```

What's to note?  `("." "")`.  This is where you specify how to copy the extracted files onto the destination system.  In this case, VS Code already has everything in good directories (or, at least, the executable is in `/bin`) so this is a hack to copy it to the root output directory.  `copy-build-system` appears to strip the top level directory from the files for what it's worth.

It works!  Running `code` in a terminal however I'm greeted with: `/run/current-system/profile/bin/code: line 53: /gnu/store/...-vscode-1.47.3/bin/../code: No such file or directory`.

The file's obviously there, and not a symlink.  Next step is to check interpreter with ldd (need to add `glibc` to my system packages section). That shows a bunch of missing libraries... cairo, libxcb, libXcomposite, gtk, at, expat, uuid, asound, etc.  Although the interpreter seems fine.  Anyway, let's try solving the library issues first.

We'll need `patchelf` for this.  This is a tool that modifies binaries to change the library search path, among other things probably.  Let's first check the rpath on `code`:

```
$ patchelf --print-rpath /gnu/store/...-vscode-1.47.3/bin/../code
$ORIGIN
```

Google time.  Okay, so that means it doesn't really have any search path info, other than "look nearby".

Now there's a good and a bad path here.  The good path would be to add dependencies on all of the necessary packages and change rpath to point to those packages, and we're taking the bad path.

