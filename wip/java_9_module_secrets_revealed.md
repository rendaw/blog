# Why Java 9 Modules?

Let's be clear - the only benefit I can see for using this mess of a system is getting `jlink` to create light weight self contained distributables of your application.

If you don't need this turn back now.  You don't need to use modules, even in Java 11!  It is a long, long, long, dark tunnel, and the light at the end is kind of dim anyway.

Uh, but if you're a library maintainer please stick around for the next couple sections and consider doing a multi-release jar for all the dumb masochists who might want to do the jlink thing and happen to use your library.

# What were they thinking alert

Java already had this mess of identifiers:

1. Maven artifacts, like `com.google.guava`
2. Jar filenames, like `guava-version.jar`
3. Packages, like `com.google.common`

And now we have module names too!

4. like `com.google.common`

All of these are completely independent, used by different systems.  Er, mostly.  Java 9 modules retroactively require that 2 jars can't contain the same module name.

Just be aware that _for the same dependency_ these can all be totally different, and _will_ be totally different in many cases.

# Okay, what do I need?

Basically, all your dependencies must be proper modules, with `module-info.java/.class`.

Automatic modules will not work!

But basically no libraries are proper modules at this point.  The rest of this article describes how you can coerce random dependencies into being properish modules and use those to jlink.

# For library maintainers

[This article](https://dzone.com/articles/automatic-module-name-calling-all-java-library-maintainers) and a number of other articles suggest adding an automatic module name has some sort of technical merit.  It doesn't - it's as good as writing a blog post saying "my module name will be x.y.z".

But adding a `module-info.java` requires Java 9 bytecode!  So it's backward compatibility or modules, either or, right?

There's a solution: You will make a multi-release jar.  This is a jar with the `module-info.class` in a location where pre-module tools will ignore it, but sufficiently modern module tooling will find it and use it as usual.  All the code is compiled for Java 6/7/8, except the module-info which is Java 9 bytecode.

1. Create a directory `src/main/java-mr/9` and create `module-info.java` in it.  You might want to add that directory to your source roots in your IDE so the module-info can be used for dependency resolution in the IDE.

    Your `module-info.java` basically just needs:
    ```
    module your.thing {
      requires some.other.module;
      exports all.your.directories;
    }
    ```

2. Add these three things to your pom.xml:

    In `<project>`:

    ```
    <packaging>multi-release-jar</packaging>
    ```

    In `<build>`:

    ```
    <extensions>
        <extension>
            <groupId>pw.krejci</groupId>
            <artifactId>multi-release-jar-maven-plugin</artifactId>
            <version>0.1.5</version>
        </extension>
    </extensions>
    ```

    In `<build><plugins>`:

    ```
    <plugin>
        <groupId>pw.krejci</groupId>
        <artifactId>multi-release-jar-maven-plugin</artifactId>
        <version>0.1.5</version>
        <configuration>
            <source>1.9</source>
            <target>1.9</target>
            <archive>
                <manifestEntries>
                    <Built-By></Built-By>
                    <Multi-Release>true</Multi-Release>
                </manifestEntries>
            </archive>
        </configuration>
    </plugin>
    ```

You're done! Publish away!

Obviously coming up with `requires` lines for non-module 3rd party dependencies is a trick.  The article above says don't do this if any of your own dependencies aren't modules or don't have explicit automatic module names (in the `MANIFEST.MF`).  I'm not convinced - most major libraries have explicit automatic module names now, the ones that don't are probably dead, maven central jar filenames don't change much, and even if you get a dependency wrong users can override it with the below steps.  And then at least some people will be able to use your stuff as is!

If you want other packages to be able to depend on your dependencies, swap `requires` for `requires transitive`.  I don't know why this isn't the default.

Also I'd swap `module` with `open module` which makes your code reflectable.  Encapsulation is in the eye of the beholder and if other libraries are worth worrying about they won't be recklessly using internal apis anyway.

# Moditect

This is only required for application distributors!  Libraries don't need to do anything except the above.

Moditect has a bunch of goals but you need to use all of them, together - there doesn't seem to be any use case for using them individually.

What moditect does:

### 1. Create module-info.class files and force them into the jars

Use the `add-module-info` goal for this.  You can either specify the `module-info` yourself or have moditect generate it automatically using `jdeps` (or a mix of the two with the selective override syntax):

```
<execution>
    <id>forcibly-java-9</id>
    <phase>prepare-package</phase>
    <goals>
        <goal>add-module-info</goal>
    </goals>
    <configuration>
        <overwriteExistingFiles>true</overwriteExistingFiles>
        <outputDirectory>${project.build.directory}/modules</outputDirectory>
        <modules>
            <module>
                <artifact>
                    <groupId>com.google.guava</groupId>
                    <artifactId>guava</artifactId>
                    <version>27.0.1-jre</version>
                </artifact>
                <moduleInfo>
                    <name>com.google.common</name>
                </moduleInfo>
            </module>
...
```

Yes, you need to specify the exact version manually.  This is crazy fragile.  Never update your dependencies or hope someone comes up with a more automatic solution.

Also to note, due to [this bug](https://bugs.openjdk.java.net/browse/JDK-8210502) `jdeps` fails on multi release dependencies.  Apparently fixed in Java 12.

You need 1 `<module>` for each non-module dependency you have, direct or transitive.

Note that the jars are output to a subdirectory of `target/` - no other plugins will consume these jars, which is why we have to also run `jlink` with moditect.

This also means that `<name>` needs to be the automatic module name because the modularized jar is only packaged, not used when compiling or doing any other analysis.  You can find out the module name by looking at error messages when you try to build and digging through the dependency source code and build files.

### 2. Run jlink

There's also the `maven-jlink-plugin`, but it can only use modules in your maven repository.  That is, if any dependency is not a Java 9 module then it will fail.

The only way to invoke jlink with automatically generated module path is to use moditect's `create-runtime-image` goal.  This will instead only use modules in `<modulePath>`:

```
<execution>
    <id>create-runtime-image</id>
    <phase>package</phase>
    <goals>
        <goal>create-runtime-image</goal>
    </goals>
    <configuration>
        <modulePath>
            <path>${project.build.directory}/modules</path>
        </modulePath>
        <modules>
            <module>your.module.name</module>
        </modules>
        <outputDirectory>
            ${project.build.directory}/jlink-image-linux-x64
        </outputDirectory>
    </configuration>
</execution>
```

But wait, only the forcibly modularized dependencies are there!  Do I manually have to add all the upstream modularized dependencies' module paths to the list?  The undocumented trick here is to use `maven-dependency-plugin` to copy all the dependencies into `target/modules`:

```
<plugin>
    <groupId>org.apache.maven.plugins</groupId>
    <artifactId>maven-dependency-plugin</artifactId>
    <version>3.1.1</version>
    <executions>
        <execution>
            <id>copy-dependencies</id>
            <phase>prepare-package</phase>
            <goals>
                <goal>copy-dependencies</goal>
            </goals>
            <configuration>
                <outputDirectory>${project.build.directory}/modules</outputDirectory>
                <includeScope>runtime</includeScope>
            </configuration>
        </execution>
    </executions>
</plugin>
```

### 3. Create a `module-info.java` for your project

This didn't work for me due to the `jdeps` bug mentioned in #1.  But maybe someday!

You don't want to use this also if anything depends on this project - your IDE won't be able to find the `module-info.java` and you'll get lots and lots of squiggly reds.

So for non-application submodules and the like you'll probably have to make the module-info manually.

# Unclear errors with no documentation!

### Error reading module: /.../target/modules/jna-platform-5.2.0.jar

Run `jlink` or whatever tool with `--verbose -J-Djlink.debug=true`, and you'll see the actual error message.

moditect shows the `jlink` command line if you add `-X -e` to your `mvn` call.

### java.lang.module.InvalidModuleDescriptorException: Package com.sun.jna.platform.win32.COM$util not found in module

The module has `exports com.sun.jna.platform.win32.COM.util` but apparently there aren't any classfiles there.  The thing that tripped me up was `COM$util` - this is obviously not a package.  And `COM/util` has plenty of classfiles.  Whatever, just remove that export.

### Error: automatic module cannot be used with jlink: javafx.graphicsEmpty

I've seen rumors that automatic modules can be used with jlink, you just need to pass some flag.  The most straightforward solution is to add this module to your `add-module-info` moditect execution.

### Error: package X is not visible

You need to `require` it in your `module-info.java` file.

### Error: module not found

Make sure your maven-compiler-plugin is fresh, version 3.8.0 or later recommended.

Also make sure the required modules are in your maven dependencies _as well as_ your module-info.java file.

### Error: access denied, class file not found

This happened to me.  The class was obviously in the jar, the jar was obviously in the class/module path (tried both, alone and together).  Moved the affected code into a new module and it suddenly started working.  Have seen reports of restarting the computer also helping.

### Various troubleshooting:

* List contents of your reduced image to see what's actually packaged: `/usr/lib/jvm/java-11-openjdk/bin/jimage list image/lib/modules`
* Copy `libjdwp.*` and `libdt_*` from your jdk `lib` directory to the reduced image `lib` directory and add `-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=5005` to your Java commandline to remote debug the image.

### Other errors

I might have seen them before, drop me a line.