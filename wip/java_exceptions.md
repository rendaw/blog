1. Never raise checked exceptions in APIs.

   Whether an exception needs to be caught or should propagate upward depends entirely on context.  Checked exceptions force them to be caught in _every_ situation (or be added to throws, which prevents abstraction).

2. To deal with checked exceptions you don't care about, catch them and rethrow them with an unchecked wrapper.

   The standard library provides `UncheckedIOException`, my commons library has a generic `UncheckedException`.  By making use of the newish exception chaining feature you won't lose any information when rethrowing.

3. Never catch `Throwable`/`Error`.

   As far as I can tell `Error` is used for two things, apocalypse events and flow control exceptions for use within a specific environment.  For instance, assertion failures.  Consuming `AssertionError` for example will prevent unit tests from failing in the case of legitimate error.  As a workaround I also have a new `Assertion` exception in my commons library which makes it through most poorly written frameworks.

3. When you need uncrashable code, catch `RuntimeException` or `Exception` if you happen to also raise checked exceptions at that scope.  If an apocalypse happens your code will crash, no matter what.

   By following rule 2 the only unchecked exceptions you need to worry about are those derived from `RuntimeException`.