Having multiple listeners on a single property is bad due to implicit ordering dependencies.

Other than being careful to register listeners in a certain order, there's a nefarious scenario of mismatched listener lifetimes.

Suppose property A has two listeners:

Listener N always exists but gets recreated periodically when another event happens.

Listener M exists since the start of the application and depends on a value calucated by listener N.

Once listener N is recreated it is added to the end of the notification queue and listener M will be unable to get the value.

The simplest readable solution to the dependency problem is to have a single listener which explicitly invokes the listeners segregated by type (ordering dependencies).  The manual nature of this makes it hard to use second order tools, like property bindings and callback combinators though.

A dependency graph and analyzer approach is difficult because 
