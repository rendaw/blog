All programming languages should have these basics:

* Orthogonality

* Introspection

    This is at least somewhat required for orthogonality.  Introspection allows you to reconcile slight differences in usage contexts to improve code reuse.  For compiled languages it's fine if introspection is only available at compile time.

    _All_ information available to the interpreter/compiler should be available, down to the statements in a parsed block.

* Modules

    * For decomposing a project
    * For using externally developed code

    This implies robust namespacing and encapsulation.

    The language should have an official way to distribute modules publically and privately, as well as download and manage modules for a project.

* Cross platform

    The core language should work/be able to be ported to other platforms and should support abstracting platform-specific details where necessary.

* Small standard library

    The standard library should have enough to bootstrap other work and no more.

    Large standard libraries become a crutch. The standard library becomes tightly integrated preventing alternative solutions from being used, the core language developer team is typically understaffed and can't understand all use cases, changes become slow because of the large dependent legacy code base and strict quality standards.

* Exceptions

    This seems contentious, but in Go you more or less cannot compose functions while still robustly recovering from errors.  You end up with almost every function call taking 4 lines: 1 to call and store the results, 3 to check for and propagate any errors from that line.

    In Lua there's not enough information in exceptions to recover since they're all strings.

# Glue languages

Glue languages are strictly for small programs that define behavior that's too difficult to define with a UI.  These languages are a convenience, and can the role can be filled by cog languages if no glue language is available.

These are small embedded languages (for _small_ plugins/macros) and scripting languages (shell scripts).  The core language should be abstract with specifics defined by the execution context: byte manipulation for Wireshark dissectors, process and filesystem interaction for shell scripts, etc.

In addition to the general language requirements, glue languages should be:

* Small

* Easy/fast to develop

    Conservative memory management and type safety are not required.

* Embeddable

    The enormous Python standard library means you either embed a nonstandard Python (parts removed) or your application becomes huge.

* Replacable with cog languages

    It's possible for any program to balloon in size to the point that you'd be better served by a cog language.

# Cog languages

Languages that can be used for writing operating systems, enterprise software, desktop applications, server software, etc.

* 2 stage

    A significant amount of development requires external code generators because single stage languages aren't powerful enough.  Things like serializer/deserializer generators (protobuf), binding generators, preprocessors, templating languages, resource embedding, symbolic processors, etc.  External code generators have one way interaction with the existing code base, break when definition files get out of sync, interfere with development tools, and have parsers strongly coupled to the language version.

    Static analysis (ex: type checking) can be thought of as a 1st stage process.

* Strong editor support/refactoring tools

    At the minimum:

    * Renaming symbols
    * Jumping to symbol definitions
    * Autocompleting fields, methods, method definitions, class definitions from an interface

# Other notes

Domain specific languages are an antipattern - there's no reason a certain language can't support all use cases.  Domain specific syntaxes may be reasonable.

Coroutines are enormously helpful and I would like to see them in every language. The main difficulty in implementing them is memory management with per-coroutine stacks.

I believe it should be possible to create a language that works on every platform, in every domain, with the same code.

Languages today are in their infancy.  Practice makes perfect, and how many people have created 10+ languages (or languages with 10+ _significant_ major revisions)?  How many people are capable of making a programming language?  It's easy to find a flawless song or painting - millions of people worldwide are creating tens of millions every day.  Creating better tools for making languages should be as much a priority as making better languages.