# A brief Guix review

[Guix-SD](https://www.gnu.org/software/guix/) (which I'm just going to call Guix from now on) is a Linux distribution featuring a declarative config-based setup with everything from base services and system glue to the config written in GNU Guile.

Is it good?

## TLDR

* The core ideas are great, and the use of a battle tested language is fantastic in practice
* I really needed the abstractions like running a system in a vm or creating a bootable image
* There are large undocumented gaps in the implementation
* The rest of the documentation is severly lacking
* The community tools (mailing list, bug tracker) are user hostile

It's usable, but be prepared to get at least neck deep in the Guix source!

## The details

### Awesome things

**The whole distribution is built in a single language.**

No hodge-podge of bash, perl, python, a little golang here, a little c there (well, there is some small amount of c, but we'll ignore that).

By virtue of using a lot of GNU software that _also_ uses Guile the configuration naturally flows together.

**Guile is a real language, with an ecosystem - libraries, tools, documentation, etc.**
There are lots of websites with tutorials, explanations, questions on SO, etc.

**The Guix project places a lot of emphasis on reproducibility**: not just of [builds for an individual package](https://www.gnu.org/software/guix/blog/2017/reproducible-builds-a-status-update/) but also [historic package](https://www.gnu.org/software/guix/blog/2019/connecting-reproducible-deployment-to-a-long-term-source-code-archive/) and operating system versions.

**The configuration file defines 100% of your system**.  No manual tweaking, final steps, etc.

**The configuration has very little boiler plate!**

**You can put your system configuration in version control and diff your changes.**

**Guix abstracts the "current system"**

   * With just your config file you can install on your current computer, or install to a VM, install to a file which you can copy onto a thumb drive, or install to produce a docker image.
   * You can often use the same config to install to both bare metal and a file/vm with - this lets you debug your new installation on a different computer and hopefully avoid the common reboot-kernelpanic-google cycle.

### Not awesome things

**The documentation**

I'll admit it's at least much much better than Nix's last I checked.

* It's 90% reference information
* The reference information lacks a lot of details.  Like, what does a function return?  Or what does this boolean value do when turned off?
* Non-reference information is mixed in with reference information.
* Fundamental concepts are not explained (file system formatting, service extension, how to execute a program, etc)
* Significant unimplemented functionality, critical caveats aren't documented

**Bugs** - in packages are a given, especially coming from Arch, but there are lots of bugs and rough points in core tools (ex: `guix system`)

**Silent misconfiguration** a lot of issues with your configuration/command are silently ignored.  You're likely to get a bootable system, but is it the system you wanted?

**The official contact point is a mailing list**

If you're not sure why a mailing list is a bad thing:
   * It's a hack on the email system using lots of hidden and nonstandard parameters for threading.
   * Since it's a hack, it's easy to break stuff and have your messages appear in the wrong place, or get lost in the ether.
   * When something goes wrong, you'll never know.
   * By default you get spam from all conversations on the list.
   * If you mess up your subscription settings you'll simply no longer see replies.  And if you don't get a message in your client it's hard to respond to the list.
   * The `Reply` button in the web interface appears to simply not work (Thunderbird opens an email with no addressees and no subject)
   * No built in search

**The bug tracker is also a pseudo mailing list**

   * It's hard to find
   * It requires you to send an email with a magical largely undocumented format
   * If you don't your bug gets lost in the ether (or at least until a poor caretaker asks you where it should have gone)
   * Arcane inscriptions as a UI pattern (what the heck does `n|E|_` mean?)
   * No full text search

**The source repository... is not modern**
   * It's hard to get to - even more so than the bug tracker.
        In "contribute" link in the top drop down, at the link titled "Savannah" in a "Project Management" section, then hover the "Source Code" link to access the "Browse Sources Repository" link
   * No code hilighting
   * No built in code search

**There's no docker image**, so you can't fully automate a project in CI, and I had issues installing the CLI on top of a Debian docker image.

**There's no unified monitoring, logging goes through syslog; no ability to search system logs other than grep.**

### Depending on your pain tolerance

I'm fairly positive about Guix.  Most of the awesome things are really awesome, the not so awesome things hopefully temporary and improving.

I've migrated one of my servers, and I'll probably slowly migrate the rest of my systems.  I have my own guide that I think fills in a lot of the documentation gaps [here](how_to_guix_for_those_who_dont.md), so please check it out!
