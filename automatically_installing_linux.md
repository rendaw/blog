# Mainstream technologies

There are three maintained well-used solutions for automating linux setup:

1. Kickstart - this is a Redhat solution.  It's also useable (somewhat?) on Ubuntu.
2. Preseed - this is the Debian solution.  It's also usable (somewhat?) on Ubuntu.
3. Ubiquity - this is the Ubuntu solution.  It's very similar to Preseed.

None of these appear to be particularly documented, but I went with Preseed since the information on the other two was too mashed up.

# Preseed

You can put your preseed on a webserver (github?) and type it in to the official installer image when you select the automatic install option.  This has some negatives:

1. You have to type it in each time
2. If you want to put secrets or whatever in it they'll be public - unless you run the webserver on a secured private network.
3. If you need additional files you'll have to also serve them and downloading them is tricky.

The alternative options is to modify the ISO to include all the changes and automatically start the installation.  This can be done for kickstart/ubiquity too of course.

# Preseed partitioning

Partitioning is always handled by partman-auto (there is no non-auto option).  You must choose a method provided by one of the `partman-auto-*` subhandlers.  For instance, `lvm` method is provided by `partman-auto-lvm`.  The subhandler in turn uses a `recipe`, which you may provide via `partman-auto/choose_recipe` and `partman-auto/expert_recipe` (the latter to define your own recipe).

partman-auto-crypto in turn will use partman-crypto.  Between the two of them they will use luks or dm-crypto, not exactly sure how it chooses (if you specify a passphrase maybe?).

lvm and crypto paritioning methods need some partitions to be specified with `$lvmok{ }` - this indicates that the partition should be created as a logical volume in the volume group.  If you don't have any `$lvmok{ }` partitions you'll get an error.

# Remastering the installation ISO

ISO images have a fixed file sizes and locations so you can't modify it in place.  The procedure here is to extract it, modify the files, then recreate the ISO.

When remaking the ISO you need to specify "eltorito" boot configurations.  If you're booting the image on a BIOS system, you need an isolinux boot configuration.  If you're booting the image on a UEFI system you need a EFI directory and related files.  You can configure both.

To allow the image to boot off a USB drive use isohybrid.  It requires a BIOS boot configuration even if you're setting it up for UEFI boot.

# Testing the image

You can script Virtualbox to create and start a VM with the installation media.

VBoxManage has an unattended option which is largely undocumented but seems tailored to solutions like this.  However it makes a lot of unlisted changes to the VM - creating and attaching it's own virtual cdrom device, changing boot order, etc.  It doesn't seem like it can be used to, say, automate preseed specification with the default ISO.

# Debugging Preseed

It took me a while to realize this, but press ALT-F4 (not CTRL-ALT-F4) to switch to the console output of the installation.  If you're using `late_command` to add custom installation steps you'll need this to see how it's failing.

It's recommended to have `late_command` just run a shell script you've added to the installation image because the `late_command` arguments don't run in a shell environment, don't allow quoting, etc.