# If you really want to Guix then just!

As the sequel to "How to Guix for those who don't", this is a bunch of snippets showing you how to set up common things in Guix.

## Disable root password

If you add your own root account, the default account won't be used.

```
 (user-account
     (name "root")
     (password "x")
     (uid 0)
     (group "root")
     (home-directory "/root"))
```

By setting invalid crypt password `x` login won't be possible.

## iptables

## Installing secure (only root-readable) files

Currently Guix doesn't support this - if you're creating a disk-image you need to do this by modifying the image after creation.

## Disable some base services

In `(services ...)` instead of `%base-services` you can use a filtered list:

```
 (filter
     (lambda (e) (not (member (service-kind e) (list
         agetty-service-type
     ))))
     %base-services)
```

This returns all services that are not `agetty-service-type` (in my case I configured my own agetty service).

## Submissions

If you have any snippets please send them this way!
