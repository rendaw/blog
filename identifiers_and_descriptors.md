# Identifiers and Descriptors

In computing, IDs (identifiers) are all over the place.  Computers, devices, websites, software, pieces of memory, files, servers, accounts, etc. all have IDs.  And almost everyone, both users and designers, conflate identifiers with descriptors causing numerous problems.

I'd like to make this issue more visible and explore some partial solutions.

### What is an identifier

Simply, an identifier is a value that allows you to tell if two things are the same or not.  If A's identifier is `SN 12345A9-C` and B's identifier is also `SN 12345A9-C`, then A is B.

### What is a descriptor

This isn't a term that's typically used, but I'm snagging it since I don't have a better alternative at the moment.  A descriptor, unlike an identifier, provides information about how a thing relates to a context (like yourself).  A and B's descriptor might be `the big fridge`, if you regularly use only a small and big fridge.  Descriptors may be different for different people (`Mary's fridge`) or in different situations (`the fridge we sold last week`), or may change over time due to various events (`the fridge from the old apartment`).

## What problems does this cause

### Usernames

Some services need usernames as a way to reference users socially, but honestly that's a very minor selection.  Your bank isn't going to say `User "mark" has $1,000,000 in his account!` on their front page.  Using a descriptor here is natural - the `name` in `username` evokes a descriptor like "Mary" or "Dmitry", and furthermore something like `mary2020` is way easier to remember than `A@3f3p7kkn`, and if you can use `mary2020` on multiple websites even better.

But you shouldn't.

* A descriptor is likely easier to guess, which makes your account more susceptible to attacks.
* If account information is leaked, it may be easier to tie the details back to you.
* Other people might want or expect your username, they may try to break in inadvertantly thinking they were the original account owner.  And this is why services ask for so much extra information (full name, birthday, security questions, phone number), exposing you to further personal information compromise.
* If you want to get rid of an account and don't have a process for remembering different account names you're in for a rough time.
* On social services, do you want people to be able to link your account to other services? Once you've exposed the connection you can't take it back.
* Who wants to be reminded how un-unique they are, and how subordinate (`username "mark" is taken, how about "mark15"`) they are to boot.

I recommend using a [random text generator](https://www.random.org/passwords/?num=1&len=20&format=html&rnd=new) to create account ids and managing them along with your super-strong passwords in a password manager like the [Trezor Password Manager](https://trezor.io/passwords/), [LastPass](https://www.lastpass.com/) or [Pass](https://www.passwordstore.org/)/[GoPass](https://www.gopass.pw/).

### Domain Names

Everyone knows domain names, `google.com`, `microsoft.com`, `linkedin.com`, `wikipedia.org` as various services' faces on the web.  But these also are actually identifiers.

* To me, Bluebird is a Javascript library for asynchronous programming.  But `bluebird.com` gives me a bank's website.
* A typo can send you to a malicious page, and in fact is [fairly common](https://en.wikipedia.org/wiki/Typosquatting)
* If a service changes their name, users will either be confused by a domain that doesn't match the website contents (looks like phishing) or the company has to implement redirects indefinitely and repeatedly encourage users to update their bookmarks.  All internal uses of the domain name must also be updated (documentation, system configuration) which is a large cost.
* Tricks such as using a long subdomain, ex: `google.com.some.other.unrelated.site` can cause users to thinking they're accessing a different site than they really are.
* Domain name conflicts - there are 17k three letter acronyms, so it's easy to imagine multiple people (or companies) have legitimate claims on the same one.

Google and other companies are trying to obviate domain names in a variety of ways - searching to find websites by description, [altogether remove the address bar](http://www.conceivablytech.com/7485/products/google-is-serious-you-can-kill-chromes-url-bar) or at least [hide parts of it](https://news.softpedia.com/news/how-to-bring-back-www-and-http-flags-in-google-chrome-69-address-bar-522640.shtml), but there isn't really a good general purpose solution.  You can use IP addresses directly, but as companies are used to relying on domain names those tend to change frequently.  [I2P](https://en.wikipedia.org/wiki/I2P) and Tor use roughly random IDs to identify nodes, the former with an [address book](https://geti2p.net/uk/docs/naming) addon for adding descriptors.

### AWS CloudFormation

In AWS CloudFormation, all resources (servers, ip addresses, buckets, etc.) have logical ids.  If you change a logical ID, CloudFormation thinks the resource with the old id has disappeared and deletes it and thinks the new id is a new resource and creates it from scratch, so it is absolutely an identifier.  But everywhere in the [documentation](https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/GettingStarted.Walkthrough.html) and on [the web](https://medium.com/boltops/a-simple-introduction-to-aws-cloudformation-part-1-1694a41ae59d) demonstrates using logical ids like `WebServer` and `EC2Instance`.  By that precedent you might name your database cluster `Database` or `MainDatabase` or somesuch.

But then:

* If you decide to use the existing database as a testing instance and set up a new instance for production, you now have `Database` and `ProdDatabase` - slightly confusing.
* If you decide that you need another instance for staging with stricter usage controls and you'd like to use the original database for development, you now have `Database`, `ProdDatabase`, and `StagingDatabase` - more confusing.
* Alternatively, maybe you make a product upgrade with a new data model, schema, database software, etc, but you need to keep the old data/service for legacy customers.  You now have `Database` and `NewDatabase`.  What if you upgrade again?  `NewNewDatabase`?
* Or maybe you only have one database but realize that `Database` is a terrible and not future-proof descriptor in the first place.

Cloudformation has some "description" parameters, but these are not consistently visible in the UI and sometimes are immutable (changing the description causes a delete + create, the same as changing the identifier).  In other places you can use a Tag to describe resources, but this is also inconsistent: many resources don't support tags, some UI's won't show tags.

This is a hard sell, but I encourage using randomly generated identifiers for AWS resources as well.  It allows you to move faster since you don't need to think about or plan id structure beforehand, and you can you can work towards a well fitting descriptor taxonomy iteratively.  Most IDEs can be easily extended to create a hotkey for random ID generation (at least, Vim and Idea can).  Then to add descriptors, the following browser user script will perform live substitutions:

```javascript
// ==UserScript==
// @name            Sub ids on aws
// @include         https://*aws.amazon.com/*
// ==/UserScript==

const subs = []

const simpleSubs = [
    ['H000000005b07d958', 'account server'],
    ['H266ee15f', 'ecs service'],
].forEach(s => subs.push([new RegExp('(' + s[0] + ')([^\\]]|$)'), '[' + s[1] + ' $1]$2']))

const recurse = (node) => {
	if (node instanceof CharacterData && !(node.parentNode instanceof HTMLInputElement)) {
		for (let sub of subs) {
			node.data = node.data.replace(sub[0], sub[1])
		}
	}
    node = node.firstChild
    while (node) {
        recurse(node)
        node = node.nextSibling
    }
}

recurse(document.body)

new MutationObserver((mutations, observer) => {
	for (let mutation of mutations) {
		if (mutation.addedNodes) {
			for (let node of mutation.addedNodes) {
				recurse(node)
			}
		} else {
			recurse(mutation.target)
		}
	}
}).observe(
	document.body,
	{subtree: true, childList: true, characterData: true}
)
```

## Why the conflation persists

When someone encounters a random identifier, they have no idea what it is.  When the identifier is a (rough) descriptor, the user can at least guess and perhaps start to connect things together.  

If you maintain a set of personal descriptors as in the CloudFormation example, you somehow need to distribute this to everyone interacting with the identifiers, they have to go through the effort of installing it, and it needs to be kept synchronized with the live id set.  If you need to access the infrastructure on the go or on an unfamiliar computer your descriptors will be missing.

From a service perspective this can be difficult as well.  Both the identifier and descriptor may be necessary, and fitting both into the layout could take significant work.  If users don't put the effort in to create descriptors, they may confuse themselves and rate the service poorly.  And providing per-user and other contextual descriptors like with the CloudFormation script above has the same maintenance issues.